// implement function for user to input an angle theta in degrees
// and output the sine and cosine of theta in degrees

#include <iostream>
#include <cmath>
#include <vector>

double getUserInputTheta()
{
    double theta;
    std::cout << "Enter an angle in degrees: ";
    std::cin >> theta;
    return theta;
}

double getUserInputAbsVelocity()
{
    double v0;
    std::cout << "Enter an absolute velocity: ";
    std::cin >> v0;
    return v0;
}

double degToRad(double deg)
{
    return deg * M_PI / 180;
}

double getVelocityX(double v0, double theta)
{
    return v0 * cos(degToRad(theta));
}

double getVelocityY(double v0, double theta)
{
    return v0 * sin(degToRad(theta));
}

std::vector<double> getVelocityVector(double v0, double theta)
{
    std::vector<double> results = {getVelocityX(v0, theta), getVelocityY(v0, theta)};
    return results;
}


int main()
{
    double theta = getUserInputTheta();
    double v0 = getUserInputAbsVelocity();
    std::vector<double> velocityVector = getVelocityVector(v0, theta);
    std::cout << "The velocity vector is (" << velocityVector[0] << ", " << velocityVector[1] << ")" << std::endl;
    return 0;
}

double getDistanceTraveled(double v0, double theta)
{
    return v0 * v0 * sin(2 * degToRad(theta)) / 9.81;
}

double targetPractice(double distanceToTarget, double velocityX, double velocityY)
{
    double distanceTraveled = getDistanceTraveled(v0, theta);
    return distanceTraveled - distanceToTarget;
}


bool checkIfDistanceToTargetIsCorrect() {
    double error = targetPractice(0, 0, 0);
    return error == 0;
}

#include <random>

double generatePseduoRandomNumber()
{
    std::random_device rd;
    std::default_random_engine generator(rd());
    std::uniform_real_distribution<double> distribution(0.0, 1.0);

    for (int i = 0; i < 100; i++)
    {
        double number = distribution(generator);
        std::cout << number << std::endl;
    }
    int number = distribution(generator);

    return number;
}

double randomWithLimits(double lowerLimit, double upperLimit)
{
    double randomNumber = generatePseduoRandomNumber();
    return randomNumber * (upperLimit - lowerLimit) + lowerLimit;
}

void playTargetPractice()
{
    double distanceToTarget = randomWithLimits(100, 1000);
    double theta = userInputTheta();
    double v0 = userInputAbsVelocity();
    std::vector<double> velocityVector = getVelocityVector(v0, theta);
    double error = targetPractice(distanceToTarget, velocityVector[0], velocityVector[1]);
    std::cout << "The distance to target is " << distanceToTarget << std::endl;
    std::cout << "The velocity vector is (" << velocityVector[0] << ", " << velocityVector[1] << ")" << std::endl;
    std::cout << "The error is " << error << std::endl;
}


